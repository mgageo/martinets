# <!-- coding: utf-8 -->
#
# quelques fonctions pour les moineaux
# auteur : Marc Gauthier
# licence: Creative Commons Paternité - Pas d'Utilisation Commerciale - Partage des Conditions Initiales à l'Identique 2.0 France
# ===============================================================
#
#
#
# lecture du fichier excel d'origine
# source("geo/scripts/martidelle.R");moineaux_xlsx_lire()
moineaux_xlsx_lire <- function() {
  library(tidyverse)
  library(readxl)
  library(janitor)
  dsn <- sprintf("%s/recensement nids moineaux.xlsx", varDir)
  df <- read_excel(dsn) %>%
    clean_names() %>%
    glimpse() %>%
    extract(numero, c("NUMERO"), "^(\\d+\\s*\\w*)", remove = FALSE) %>%
    replace_na(list(NUMERO = '')) %>%
    mutate(NUMERO = trimws(NUMERO)) %>%
    extract(adresse, c("VOIE"), "^([^(]*)", remove = FALSE) %>%
    extract(adresse, c("TYPE"), "\\(([^)]*)", remove = FALSE) %>%
    mutate(VOIE = sprintf("%s %s", TYPE, VOIE)) %>%
    mutate(VOIE = trimws(VOIE)) %>%
    mutate(ligne=1:nrow(.)) %>%
    glimpse()
  carp("dsn: %s nrow: %s", dsn, nrow(df))
#  df1 <- df %>%
#    dplyr::select(ligne, NUMERO, numero)
#  print(knitr::kable(df1, format = "pipe"));stop("****")
  return(invisible(df))
}
