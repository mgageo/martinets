# <!-- coding: utf-8 -->
#
# quelques fonctions pour les données faune
# auteur : Marc Gauthier
# licence: Creative Commons Paternité - Pas d'Utilisation Commerciale - Partage des Conditions Initiales à l'Identique 2.0 France
# ===============================================================
#
fauneDir <- sprintf("%s/faune", varDir)
dir.create(fauneDir, showWarnings = FALSE, recursive = TRUE)
#
## les traitements journaliers
#
# les traitements suite à une actualisation de faune-bretagne
#
faune_jour <- function(force = FALSE) {
  carp("début")
  if (force == TRUE) {
    faune_groupes_export(force)
  }
}
# source("geo/scripts/martidelle.R");faune_groupes_export()
faune_groupes_export <- function(force = TRUE) {
  carp()
  library(tidyverse)
#  glimpse(WGS84_ullr); exit;
  carp("début")
  df <- read.table(text="code,libelle
45,hirondelles
38,martinets
", header=TRUE, sep=",", blank.lines.skip = TRUE, stringsAsFactors=FALSE, quote="")
  for(i in 1:nrow(df) ) {
    code <- df$code[i]
    if ( grepl('^#', code) ) {
      next
    }
    libelle <- df$libelle[i]
#
# interrogation des données avec medias
    dsn <- sprintf("!s/faune_%s_!s.html", code) %>%
      gsub("!", "%", .)
    carp("dsn: %s", dsn)
    biolo94_export_groupe(groupe = code, dsn = dsn, force = TRUE, Format = "XLSX") %>%
      glimpse()
    next
    dsn <- sprintf("%s/faune_export_%s.xlsx", fauneDir, libelle)
    biolo_export_groupe(groupe = code, dsn = dsn, force = TRUE)
    next
    dsn <- sprintf("%s/faune_export_%s.json", fauneDir, libelle)
    biolo_export_groupe(groupe = code, dsn = dsn, force = TRUE, Format = "JSON")

  }
}
#
# lecture des exports faune
#
# source("geo/scripts/martidelle.R"); df <- faune_exports_lire(force = TRUE)
faune_exports_lire <- function(force = FALSE, libelle = "martidelle") {
  carp("début")
  library(tidyverse)
  if(exists("faune_martidelle.df") & force == FALSE) {
    return(invisible(faune_martidelle.df))
  }
  files <- list.files(fauneDir, pattern = "^faune_export.*\\.xlsx$", full.names = TRUE, ignore.case = TRUE)
# fusion des différents fichiers
  df1 <- data.frame()
  for (i in 1:length(files) ) {
    file <- files[i]
    df <- faune_export_lire(file, force = TRUE)
    df1 <- rbind(df1, df)
  }
  dsn <- sprintf("%s/faune/faune_export_%s.xlsx", varDir, libelle)
  faune_martidelle.df <<- df1
  return(invisible(df1))
}
# source("geo/scripts/martidelle.R"); df <- faune_lire(force = TRUE)
faune_lire <- function(force = FALSE) {
  carp("début")
  library(tidyverse)
  df <- faune_exports_lire(force = force) %>%
#    filter(media_has_media %in% c("Oui")) %>%
    filter(! precision %in% c("Lieu-dit")) %>%
    filter(municipality %in% c("Rennes", "Cesson-Sévigné")) %>%
    glimpse()
  return(invisible(df))
}
# conversion en spatial
faune_lire_sf <- function(force = FALSE) {
  carp("début")
  library(sf)
  if(exists("faune_martidelle.sf") & force == FALSE) {
    return(invisible(faune_martidelle.sf))
  }
  df <- faune_lire(force)
  nc <- st_as_sf(df, coords = c("lon", "lat"), crs = 4326, remove = FALSE) %>%
    st_transform(2154)
  faune_martidelle.sf <<- nc
  return(invisible(nc))
}
#
# récupérations des médias
# source("geo/scripts/martidelle.R"); df <- faune_exports_medias(force = TRUE)
faune_exports_medias <- function(force = FALSE, libelle = "martidelle") {
  carp("début")
  library(tidyverse)
  if(exists("faune_exports_medias.df") & force == FALSE) {
    return(invisible(faune_exports_medias.df))
  }
  biolo_session(force = TRUE)
  df <- faune_lire(force = force) %>%
    filter(media_has_media %in% c("Oui")) %>%
    dplyr::select(id_sighting, media_has_media) %>%
    glimpse()
  df2 <- data.frame()
  for (i in 1:nrow(df)) {
    df1 <- biolo54_export_detail(id = df[i, "id_sighting"], force = force) %>%
      mutate(id = df[i, "id_sighting"])
    df2 <- rbind(df2, df1)
  }
  glimpse(df2)
  faune_exports_medias.df <<- df2
}
#
## le géocodage des observations
# l'id sert d'identifiant
# source("geo/scripts/martidelle.R"); df <- faune_geocode(force = TRUE)
faune_geocode <- function() {
  carp("début")
  library(tidyverse)
  df <- faune_lire(force = force) %>%
    dplyr::select(id = id_sighting, lon, lat) %>%
    glimpse()
  dsn <- sprintf("%s/reverse.csv", varDir)
  rio::export(df, dsn)
  version <- "api"
  f_dest <- sprintf("%s/%s_geo.csv", varDir, version)
  geocode_reverse_csv_datagouv(dsn, f_dest, url = "https://api-adresse.data.gouv.fr")
}
# source("geo/scripts/martidelle.R"); df <- faune_geocode_lire()
faune_geocode_lire <- function() {
  carp("début")
  library(tidyverse)
  version <- "api"
  f_dest <- sprintf("%s/%s_geo.csv", varDir, version)
  geo.df <<- rio::import(f_dest, encoding = "UTF-8") %>%
    dplyr::select(id, label = result_label, distance = result_distance) %>%
    mutate(id = as.character(id)) %>%
    glimpse()
  return(invisible(geo.df))
}