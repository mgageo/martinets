# <!-- coding: utf-8 -->
#
# quelques fonction pour les martinets hirondelles
# auteur : Marc Gauthier
# licence: Creative Commons Paternité - Pas d'Utilisation Commerciale - Partage des Conditions Initiales à l'Identique 2.0 France
# ===============================================================
#
# les fichiers umap
#
# rm(list=ls()); source("geo/scripts/martinets.R");umap_jour()
umap_jour <- function() {
  carp()
  umap_sous_quartiers()
}

umap_rennes_lire <- function() {
  carp()
  library(sf)
  if ( exists("rennes.sf") ) {
    return()
  }
  lpo_mailles <<- c("13", "16", "21", "22", "26", "27", "31", "34", "48", "49", "54")
  lpo_insee <<- c("35238", "35278")
  dsn <- sprintf("%s/opendatarennes/codes-insee-et-postaux-des-communes-de-rennes-metropole-geolocalisees.geojson", rennesDir);
  insee.sf <- st_read(dsn)
  insee.sf <<- st_transform(insee.sf, "+init=epsg:2154")
  dsn <- sprintf("%s/opendatarennes/perimetres-des-12-quartiers-de-la-ville-de-rennes.geojson", rennesDir);
  quartiers.sf <- st_read(dsn)
  quartiers.sf <<- st_transform(quartiers.sf, "+init=epsg:2154")
  dsn <- sprintf("%s/opendatarennes/perimetres-des-45-sous-quartiers-de-la-ville-de-rennes.geojson", rennesDir);
  sous_quartiers.sf <- st_read(dsn)
  sous_quartiers.sf <<- st_transform(sous_quartiers.sf, "+init=epsg:2154")
  dsn <- sprintf("%s/grille_lambert_1000.json", rennesDir)
  grille.sf <- st_read(dsn)
  grille.sf <<- st_transform(grille.sf, "+init=epsg:2154")
  dsn <- sprintf("%s/voies/troncons-de-voies-du-referentiel-voies-et-adresses-de-rennes-metropole.geojson", rennesDir);
  voies.sf <- st_read(dsn)
  voies.sf <<- st_transform(voies.sf, "+init=epsg:2154")
  rennes.sf <<- 'toto'
}
#
# le fichier geojson des sous-quartiers
umap_sous_quartiers <- function() {
  carp()
  library(tidyverse)
  library(sf)
  umap_rennes_lire()
  nc <- sous_quartiers.sf %>%
    dplyr::select(name=nomsquart) %>%
    st_transform(4326) %>%
    glimpse()
  dsn <- sprintf("%s/sous_quartiers.geojson", webDir)
  st_write(nc, dsn, delete_dsn=TRUE)
}
#
# le fichier de données en provenance du Drive
# rm(list=ls()); source("geo/scripts/martinets.R");umap_drive_donnees_get()
umap_drive_donnees_get <- function() {
  carp()
  library(googledrive)
#  (x <- drive_get("~/ONC35/observateurs"))
  dsn <- sprintf("%s/donnees.xlsx", varDir)
  drive_download(
    'donnees',
    path = dsn,
    overwrite = TRUE
  )
}
# rm(list=ls()); source("geo/scripts/martinets.R");umap_drive_donnees_lire()
umap_drive_donnees_lire <- function() {
  carp()
  library(rio)
  library(janitor)
#  (x <- drive_get("~/ONC35/observateurs"))
  dsn <- sprintf("%s/donnees.xlsx", varDir)
  df <- import(dsn) %>%
    clean_names() %>%
    mutate(lat=as.numeric(str_extract(coordonnees, '^[-+]?[0-9]*\\.?[0-9]*'))) %>%
    mutate(lon=as.numeric(str_extract(coordonnees, '[-+]?[0-9]*\\.?[0-9]*$'))) %>%
    glimpse()
  nc <- st_as_sf(df, coords = c("lon", "lat"), crs = 4326) %>%
    dplyr::select(espece, adresse, en_vol, nid)
  dsn <- sprintf("%s/donnees.geojson", webDir)
  write_sf(nc, dsn = dsn, delete_dsn = TRUE)
  carp("dsn: %s", dsn)
}
